package Frame;


import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Scanner;

import javax.swing.*;

import Controller.Control;

public class  Frame extends JFrame {
	
	private static final int FRAME_WIDTH = 700;
	private static final int FRAME_HEIGHT = 200;
	Control control;
	
	private JLabel label;
	private JLabel label1;
	private JTextArea area;
	private JTextField field;
	private ButtonGroup group;
	private JButton button;
	private JRadioButton jbutton1;
	private JRadioButton jbutton2;
	private JPanel panel1;
	private JPanel panel2;
	private JLabel resultLabel;
	
	public static void main(String[] args) {
		Control ct = new Control();
		Frame j = new Frame(ct);
		
		
	}

	public Frame(Control control) {
		this.control = control;
		setLayout(new GridLayout(1,3));
		area = new JTextArea(5,20);
		label = new JLabel("");
		label.setSize(200, 300);
		
		label1 = new JLabel("Enter number:");
		field = new JTextField("0");
		
		button = new JButton("Submit");
	
		jbutton1 = new JRadioButton("Generate Ngarm");
		jbutton1.setSelected(true);
		jbutton2 = new JRadioButton("Count Word");
		group = new ButtonGroup();
		group.add(jbutton1);
		group.add(jbutton2);
		panel1 = new JPanel();
		panel1.setLayout(new GridLayout(5,1));
		panel1.add(jbutton2);
		panel1.add(jbutton1);
		panel1.add(label1);
		panel1.add(field);
		panel1.add(button);
		

		
		
		button.addActionListener(new AddInterestListener());
		add(panel1);
		add(area);
		add(label);

		setSize(FRAME_WIDTH, FRAME_HEIGHT);
		setVisible(true);
	

	}
	
	class AddInterestListener implements ActionListener
    {
       public void actionPerformed(ActionEvent event)
       {
    	   if(jbutton2.isSelected()){
    		   if(area.getText().equals("")){
     			  JOptionPane.showMessageDialog(null, "Please Enter text");
     		  }else{
    		   String ans = control.countWord(area.getText()); 
    		   label.setText(ans);
     		  }
    	   }
    	  else{
    		  if(Integer.parseInt(field.getText())<1){
    			  JOptionPane.showMessageDialog(null, "Please Enter Number more than 0");
    		  }
    		  
    		  else{
    		   String ans = control.generateNgram(area.getText(),
    				   Integer.parseInt(field.getText())); 
    		   label.setText(ans);
    		  }
    	   }

       }            
    }
	

	

}
