package Controller;

import Frame.Frame;
import Model.Tokenizer;

public class Control {
	Tokenizer token = new Tokenizer();
	
	public String countWord(String s){
		return Integer.toString(token.countWords(s));
	}
	
	public String generateNgram(String s,int n){
		return token.generateNgram(s, n).toString();
	}

}
