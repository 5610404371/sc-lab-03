package Model;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.StringTokenizer;


public class Tokenizer {

	
	public int countWords(String s){
		ArrayList<String> list = new ArrayList<String>(); 
		StringTokenizer st = new StringTokenizer(s);
		while(st.hasMoreTokens()){
			list.add(st.nextToken());
		}
		return list.size();
	}
	
	public ArrayList generateNgram(String s,int n){
		StringTokenizer st = new StringTokenizer(s);
		String g = "";
		while(st.hasMoreTokens()){
			g+=st.nextToken();
		}
		ArrayList<String> a = new ArrayList<String>();
		for(int i = 0; i<=g.length()-n;i++){
			a.add(g.substring(i,i+n));
		}
		return a;
	}
}
